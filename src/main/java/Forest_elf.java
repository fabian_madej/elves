public class Forest_elf extends Elf implements Climb{
    @Override
    public void climb(Tree tree) {
        System.out.println(getName()+" climb on the "+tree.getName());
    }
}