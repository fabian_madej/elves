public class Sea_elf extends Elf implements Swim {
    @Override
    public void swim(Reservoir reservoir) {
        System.out.println(getName()+" is swimming in "+reservoir.getName());
    }
}