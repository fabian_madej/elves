public class Main {

    private static final int HOW_MANY = 10;
    private static Elf[] elves = new Elf[HOW_MANY];
    private static Reservoir[] reservoirs = new Reservoir[HOW_MANY];
    private static Tree[] trees = new Tree[HOW_MANY];

    public static void main(String[] args) {

        newReservoir();
        newTree();
        newElves();

        for(int i=0; i<elves.length; i++){
            System.out.print("Name: "+elves[i].getName()+" Age: "+elves[i].getAge());
            if(elves[i] instanceof Climb){
                System.out.println(" can climb");
                ((Climb) elves[i]).climb(trees[i]);
            }
            if(elves[i] instanceof Swim){
                System.out.println(" can swim");
                ((Swim) elves[i]).swim(reservoirs[i]);
            }
        }
    }
    private static void newElves(){
        for(int i=0; i<elves.length;i++) {
            if (i == 0) {
                Forest_elf newElf = new Forest_elf();
                newElf.setName("Elf_" + (i + 1));
                newElf.setAge(i + 100);
                elves[i] = newElf;
            } else {
                if (elves[i - 1] instanceof Forest_elf) {
                    Sea_elf newElf = new Sea_elf();
                    newElf.setName("Elf_" + (i + 1));
                    newElf.setAge(i + 100);
                    elves[i] = newElf;
                } else {
                    Forest_elf newElf = new Forest_elf();
                    newElf.setName("Elf_" + (i + 1));
                    newElf.setAge(i + 100);
                    elves[i] = newElf;
                }
            }
        }
    }
    private static void newReservoir(){
        for(int i=0; i<reservoirs.length;i++) {
            Reservoir new_reservoir = new Reservoir();
            new_reservoir.setName("Reservoir "+(i+1));
            reservoirs[i]=new_reservoir;
        }
    }
    private static void newTree(){
        for(int i=0; i<trees.length;i++) {
            Tree new_tree = new Tree();
            new_tree.setName("Tree "+(i+1));
            trees[i]=new_tree;
        }
    }
}